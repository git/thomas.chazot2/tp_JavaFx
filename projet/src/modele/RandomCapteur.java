package modele;

import javafx.beans.InvalidationListener;

import java.util.Random;

@Deprecated
public class RandomCapteur extends UnitCapteur{

    public RandomCapteur(float temperature, String nom) {
        super(temperature, nom, new RandomGenerator());
    }

    public RandomCapteur(Capteur capteur) {
        super(capteur);
    }

    @Override
    public void genTemp() {
        Random random = new Random();
        setTemperature(random.nextFloat(50));
    }
}
