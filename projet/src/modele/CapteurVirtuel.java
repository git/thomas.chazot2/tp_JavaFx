package modele;

import javafx.beans.property.ListProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.control.TreeItem;

public class CapteurVirtuel extends Capteur{

    public CapteurVirtuel(float temp, String nom) {
        super(temp, nom);
    }

    private ObservableMap<Float, Capteur> capteurObs = FXCollections.observableHashMap();
    private final MapProperty<Float, Capteur> lesCapteurs = new SimpleMapProperty<>(capteurObs);
    public ObservableMap<Float, Capteur> getLesCapteurs() {return lesCapteurs.getValue();}


    public void addCapteur(Capteur capteur, Float poids) {
        capteurObs.put(poids, capteur);
    }

    @Override
    public void genTemp() {
        float tempTotal=0;
        if (getLesCapteurs().size()==0){
            setTemperature(0);
            return;
        }
        for (ObservableMap.Entry<Float, Capteur> map : capteurObs.entrySet()) {
            tempTotal+=map.getValue().getTemperature()*map.getKey();
        }
        if (getLesCapteurs().size()==0) return;
        tempTotal=tempTotal/getLesCapteurs().size();
        setTemperature(tempTotal);
    }

    @Override
    public TreeItem<Capteur> accept(Visitor v){
        return (TreeItem<Capteur>) v.visit(this);
    }
}
