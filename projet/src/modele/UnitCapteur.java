package modele;

import javafx.scene.control.TreeItem;

import java.io.IOException;

public class UnitCapteur extends Capteur{

    private CaptorGeneratorStrategy captorGeneratorStrategy;

    public UnitCapteur(float temperature, String nom, CaptorGeneratorStrategy captorGeneratorStrategy) {
        super(temperature,nom);
        this.captorGeneratorStrategy=captorGeneratorStrategy;
    }

    public void setCaptorGeneratorStrategy(CaptorGeneratorStrategy captorGeneratorStrategy) {
        this.captorGeneratorStrategy = captorGeneratorStrategy;
    }

    public UnitCapteur(Capteur capteur) {
        super(capteur);
    }
    public void genTemp() throws IOException {
        setTemperature(captorGeneratorStrategy.generate());
    }

    @Override
    public TreeItem<Capteur> accept(Visitor v){
        return (TreeItem<Capteur>) v.visit(this);
    }
}
