package modele;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class Stub {
    public static Capteur genererCapteur() {
        CapteurVirtuel retour = new CapteurVirtuel(45, "cap1");
        retour.addCapteur(new CpuCapteur(12, "cap2"), 12F);
        retour.addCapteur(new CpuCapteur(0, "cap3"), 7F);
        retour.addCapteur(new CpuCapteur(20 ,"cap4"), 6F);
        retour.addCapteur(new CpuCapteur(5, "cap5"), 4.5F);
        retour.addCapteur(new RandomCapteur(25,"cap6"), 3F);
        retour.addCapteur(new RandomCapteur(40,"cap7"), 1F);
        retour.addCapteur(new RandomCapteur(30,"cap8"), 12F);

        return retour;
    }

    public static ObservableList<Capteur> genererObservableList(){
        List<Capteur> capteurs = new ArrayList<>();
        CapteurVirtuel retour = new CapteurVirtuel(45, "cap1");
        retour.addCapteur(new UnitCapteur(12, "cap2", new CpuGenerator()), 3F);
        retour.addCapteur(new UnitCapteur(12, "cap3", new RandomGenerator()), 2F);
        retour.addCapteur(new UnitCapteur(12, "cap4", new RandomGenerator()), 1F);
        retour.addCapteur(new UnitCapteur(12, "cap5", new CpuGenerator()), 4F);
        CapteurVirtuel cap=new CapteurVirtuel(12, "capVir");
        cap.addCapteur(new UnitCapteur(15, "test2", new CpuGenerator()), 5F);
        retour.addCapteur( cap, 4F);
        capteurs.add(retour);
        capteurs.add(new UnitCapteur(12, "cap6", new RandomGenerator()));
        capteurs.add(new UnitCapteur(12, "cap7", new CpuGenerator()));
        capteurs.add(new UnitCapteur(12, "cap8", new RandomGenerator()));

        return FXCollections.observableList(capteurs);
    }
}
