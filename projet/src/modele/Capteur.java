package modele;

import javafx.beans.property.*;
import javafx.scene.control.TreeItem;

import java.io.IOException;
import java.util.UUID;

public abstract class Capteur {

    public Capteur(float temp, String nom) {

        this.temperature.set(temp);
        this.nom.set(nom);
    }

    public Capteur(Capteur capteur) {
        this.temperature.set(capteur.getTemperature());
        this.nom.set(capteur.getNom());
        this.id.set(capteur.getId());
    }

    private final FloatProperty temperature = new SimpleFloatProperty();
        public float getTemperature() {return temperature.get();}
        public FloatProperty temperatureProperty() {return temperature;}
        public void setTemperature(float temperature) {this.temperature.set(temperature);}

    private final StringProperty nom = new SimpleStringProperty();
        public String getNom() {return nom.get();}
        public StringProperty nomProperty() {return nom;}
        public void setNom(String nom) {this.nom.set(nom);}

    private final StringProperty id = new SimpleStringProperty(UUID.randomUUID().toString());
        public String getId() {return id.get();}
        public StringProperty idProperty() {return id;}
        public void setId(String id) {this.id.set(id);}


    public abstract void genTemp() throws IOException;

    public abstract <T> T accept(Visitor<T> visitor);

    @Override
    public boolean equals(Object capteur) {
        return getId().equals(((Capteur) capteur).getId());
    }
}

