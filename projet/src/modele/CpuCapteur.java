package modele;

import javafx.beans.InvalidationListener;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Deprecated
public class CpuCapteur extends UnitCapteur{

    public CpuCapteur(float temperature, String nom) {
        super(temperature,nom, new CpuGenerator());
    }

    public CpuCapteur(Capteur capteur) {
        super(capteur);
    }

    @Override
    public void genTemp() throws IOException {
        setTemperature(Float.parseFloat(Files.readString(Path.of("/sys/class/thermal/thermal_zone2/temp"))));
    }
}
