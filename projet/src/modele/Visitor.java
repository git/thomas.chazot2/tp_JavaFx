package modele;

public interface Visitor<T> {
    T visit(UnitCapteur cap);
    T visit(CapteurVirtuel cap);
}
