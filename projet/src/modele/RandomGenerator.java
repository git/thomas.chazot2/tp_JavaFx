package modele;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

public class RandomGenerator implements CaptorGeneratorStrategy{


    @Override
    public float generate() throws IOException {
        Random random = new Random();
        return random.nextFloat(-20, 50);
    }
}