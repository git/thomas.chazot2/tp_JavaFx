package modele;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;

import java.io.IOException;
@Deprecated
public class CapteurImage extends UnitCapteur{

    private final ObjectProperty<Image> image = new SimpleObjectProperty<>();
        public ObjectProperty<Image> imageProperty() {return image;}
        public void setImage(Image image) {this.image.set(image);}

    public CapteurImage(float temp, String nom) {
        super(temp, nom, new CpuGenerator());
    }

    public CapteurImage(Capteur capteur){
            super(capteur);
    }

    @Override
    public void genTemp(){
        setTemperature(12);
        if (getTemperature()<0){
            setImage(new Image("/Assets/hilarious-snow-memes.jpg"));
        }
        else if(getTemperature()<22){
            setImage(new Image("https://www.francetvinfo.fr/pictures/sbviI2swQTsA1oabZGCUO9wWajQ/1200x900/2017/08/30/phppmKyIc_1.jpg"));
        }
        else{
            setImage(new Image("/Assets/this-is-fine.png"));
        }
    }
}
