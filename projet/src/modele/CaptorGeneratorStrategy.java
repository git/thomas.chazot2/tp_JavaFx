package modele;

import java.io.IOException;

public interface CaptorGeneratorStrategy {

    float generate() throws IOException;
}
