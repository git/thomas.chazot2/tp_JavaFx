package modele;

import javafx.scene.control.TreeItem;

public class TreeViewVisitor implements Visitor<TreeItem<Capteur>>{
    @Override
    public TreeItem<Capteur> visit(UnitCapteur cap) {
        return new TreeItem<>(cap);
    }

    @Override
    public TreeItem<Capteur> visit(CapteurVirtuel cap) {
        TreeItem<Capteur> item = new TreeItem<>(cap);
        for (Capteur captor: cap.getLesCapteurs().values()) {
            item.getChildren().add(captor.accept(this));
        }
        return item;
    }
}
