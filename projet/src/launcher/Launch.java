package launcher;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.stage.Stage;
import modele.Capteur;
import modele.CapteurImage;
import modele.CapteurVirtuel;
import modele.Stub;
import view.TreeViewCapteur;
import view.ViewCapteurImage;
import view.ViewCapteurVirtuel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Launch extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {

        CapteurImage capteurImage = new CapteurImage(-42F, "salut");
        CapteurVirtuel leCapteurVirtuel = (CapteurVirtuel) Stub.genererCapteur();
        ObservableList<Capteur> capteurs = Stub.genererObservableList();
        TreeViewCapteur treeViewCapteur = new TreeViewCapteur(capteurs,"/fxml/TreeViewCapteur.fxml","Mon capteur");

        Thread t = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    Platform.runLater(() -> {
                        for (Capteur capteurs1 : capteurs) {
                            try {
                                if (capteurs1 instanceof CapteurVirtuel){
                                    for (Capteur capteur: ((CapteurVirtuel) capteurs1).getLesCapteurs().values()) {
                                        capteur.genTemp();
                                    }
                                }
                                capteurs1.genTemp();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                        //capteurImage.genTemp(r.nextFloat(-20,50));
                    });

                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        t.start();

    }

    public static void main(String args[]) {
        Application.launch(args);
    }
}