package view;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.util.converter.NumberStringConverter;
import modele.Capteur;
import modele.CapteurVirtuel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ViewCapteurVirtuel extends FxmlWindow {

    public ViewCapteurVirtuel(CapteurVirtuel leCapteur, String url, String title) throws IOException {
        super(url,title);
        this.leCapteur = leCapteur;
        ListProperty<Capteur> lesCapteurs = new SimpleListProperty<>(FXCollections.observableList(new ArrayList<>(leCapteur.getLesCapteurs().values())));
        listeDesCapteurs.itemsProperty().bind(lesCapteurs);
        temperatureCapteurVirtuel.textProperty().bindBidirectional(leCapteur.temperatureProperty(), new NumberStringConverter());
    }

    @FXML
    private ListView<Capteur> listeDesCapteurs;
    @FXML
    private TextArea temperatureCapteur;
    @FXML
    private TextArea temperatureCapteurVirtuel;
    private Capteur leCapteur;

    public void initialize() {
        listeDesCapteurs.setCellFactory((param) ->
                new ListCell<>() {
                    @Override
                    protected void updateItem(Capteur item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            textProperty().bind(item.temperatureProperty().asString());
                        } else {
                            textProperty().unbind();
                            setText("");
                        }
                    }
                });

        listeDesCapteurs.getSelectionModel().selectedItemProperty().addListener((__,old,newV)->{
            if (old != null) {
                temperatureCapteur.textProperty().unbindBidirectional(old.temperatureProperty());
            }
            if (newV != null) {
                temperatureCapteur.textProperty().bindBidirectional((Property<String>) newV.temperatureProperty().asString());
            }
        });

    }

}
