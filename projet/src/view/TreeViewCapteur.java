package view;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.converter.NumberStringConverter;
import modele.*;

import java.io.IOException;

public class TreeViewCapteur extends FxmlWindow {

    @FXML
    private TreeView<Capteur> tree;
    @FXML
    private TextField name;
    @FXML
    private Text id;
    @FXML
    private TextField temperature;
    @FXML
    private VBox vBox;
    private Button textView;
    private Button imageView;
    private TableView<Capteur> tableView;
    private Button changeToCpu;
    private Button changeToRandom;
    private ObservableList<Capteur> lesCapteurs;

    public TreeViewCapteur(ObservableList<Capteur> lesCapteurs, String url, String title) throws IOException {
        super(url, title);
        this.lesCapteurs = lesCapteurs;
        changeToCpu = new Button("Change to CPU");
        changeToRandom = new Button("Change to Random");
        textView = new Button("See In Text View");
        imageView = new Button("See In Image View");

        TreeItem<Capteur> root = new TreeItem<>();
        tree.setRoot(root);
        tree.setShowRoot(false);
        tree.setCellFactory(capteurTreeView -> new TreeCell<>() {
            @Override
            protected void updateItem(Capteur item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    textProperty().bind(item.nomProperty());
                } else {
                    textProperty().unbind();
                    setText("");
                }
            }
        });
        for (Capteur capteur : lesCapteurs) {
            root.getChildren().add(capteur.accept(new TreeViewVisitor()));
        }

        tableView = new TableView<>();
        TableColumn<Capteur, String> nameCol = new TableColumn<>("Nom");
        TableColumn<Capteur, Float> tempCol = new TableColumn<>("Température");
        nameCol.setCellValueFactory(new PropertyValueFactory<>("nom"));
        tempCol.setCellValueFactory(new PropertyValueFactory<>("temperature"));

        tableView.getColumns().addAll(nameCol, tempCol);

    }


    public void initialize() {

        tree.getSelectionModel().selectedItemProperty().addListener((observableValue, old, newV) -> {
            if (old != null) {
                name.textProperty().unbindBidirectional(old.getValue().nomProperty());
                id.textProperty().unbindBidirectional(old.getValue().idProperty());
                temperature.textProperty().unbindBidirectional(old.getValue().temperatureProperty());

                textView.setOnAction(null);
                imageView.setOnAction(null);
                vBox.getChildren().removeAll(textView, imageView);

                if (old.getValue() instanceof CapteurVirtuel) {
                    tableView.getItems().removeAll(((CapteurVirtuel) old.getValue()).getLesCapteurs().values());
                    vBox.getChildren().remove(tableView);
                } else {
                    changeToCpu.setOnAction(null);
                    changeToRandom.setOnAction(null);
                    vBox.getChildren().removeAll(changeToCpu, changeToRandom);
                }

            }
            if (newV != null) {
                name.textProperty().bindBidirectional(newV.getValue().nomProperty());
                id.textProperty().bindBidirectional(newV.getValue().idProperty());
                temperature.textProperty().bindBidirectional(newV.getValue().temperatureProperty(), new NumberStringConverter());

                EventHandler<ActionEvent> eventTextView = e -> {
                    try {
                        new ViewCapteurText(newV.getValue(), "/fxml/CapteurText.fxml", "TextView");
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                };

                EventHandler<ActionEvent> eventImageView = e -> {
                    try {
                        new ViewCapteurImage(newV.getValue(), "/fxml/ImageCapteur.fxml", "TextView");
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                };

                textView.setOnAction(eventTextView);
                imageView.setOnAction(eventImageView);

                vBox.getChildren().addAll(textView, imageView);



                if (newV.getValue() instanceof CapteurVirtuel) {
                    tableView.getItems().addAll(((CapteurVirtuel) newV.getValue()).getLesCapteurs().values());
                    vBox.getChildren().add(tableView);
                } else {
                    EventHandler<ActionEvent> eventCpu = e -> {
                        ((UnitCapteur) newV.getValue()).setCaptorGeneratorStrategy(new CpuGenerator());
                    };
                    EventHandler<ActionEvent> eventRandom = e -> {
                        ((UnitCapteur) newV.getValue()).setCaptorGeneratorStrategy(new RandomGenerator());
                    };
                    changeToCpu.setOnAction(eventCpu);
                    changeToRandom.setOnAction(eventRandom);
                    vBox.getChildren().addAll(changeToCpu, changeToRandom);
                }
            }
        });
    }

}
