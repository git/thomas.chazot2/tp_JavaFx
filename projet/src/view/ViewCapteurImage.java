package view;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.util.converter.NumberStringConverter;
import modele.Capteur;
import modele.CapteurImage;
import modele.CapteurVirtuel;

import java.io.IOException;

public class ViewCapteurImage extends FxmlWindow {


    @FXML
    private Text temperature;

    @FXML
    private ImageView image;

    public ViewCapteurImage(Capteur leCapteur, String url, String title) throws IOException {
        super(url, title);
        temperature.textProperty().bindBidirectional(leCapteur.temperatureProperty(), new NumberStringConverter());
        chooseImage((int) leCapteur.getTemperature());
        leCapteur.temperatureProperty().addListener(((observableValue, oldV, newV) -> {
            chooseImage(newV.intValue());
        }));

    }

    private void chooseImage(int value){
        if (value<0){
            image.setImage(new Image("/Assets/hilarious-snow-memes.jpg"));
        }
        else if (value<20){
            image.setImage(new Image("https://www.francetvinfo.fr/pictures/sbviI2swQTsA1oabZGCUO9wWajQ/1200x900/2017/08/30/phppmKyIc_1.jpg"));
        }
        else {
            image.setImage(new Image("/Assets/this-is-fine.png"));
        }
    }


}
