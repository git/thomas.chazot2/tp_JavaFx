package view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class FxmlWindow extends Stage {

    public FxmlWindow(String url, String title) throws IOException {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(url));
        loader.setController(this);
        setScene(new Scene(loader.load()));
        show();
    }
}
