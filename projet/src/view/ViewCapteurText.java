package view;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.util.converter.NumberStringConverter;
import modele.Capteur;

import java.io.IOException;



public class ViewCapteurText extends FxmlWindow {

    @FXML
    private Text temperatureCapteur;

    public ViewCapteurText(Capteur leCapteur, String url, String title) throws IOException {
        super(url, title);

        temperatureCapteur.textProperty().bindBidirectional(leCapteur.temperatureProperty(), new NumberStringConverter());
    }
}
